/************************************************************************************/
// exer11.js ::: funcions per a l'exercici 11
// Implementaci� d'una calculadora senzilla
//
// Data �ltima revisi�: 09/05/2007
/************************************************************************************/
// Copyright (C) 2007  M�nica Ram�rez Arceda
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
/************************************************************************************/

var subtotal = 0;
var premop = true;
var opAnt = '+';

function numero(num){
	
	if(premop == false)
		document.getElementById("pant").value = document.getElementById("pant").value + num;
	else
		document.getElementById("pant").value = num;
		
	premop = false;
}

function opera(op){


   switch(opAnt){
		case '+':
			subtotal = subtotal + parseFloat(document.getElementById("pant").value);
		break;
		case '-':
			subtotal = subtotal - parseFloat(document.getElementById("pant").value);
		break;
		case '*':
			subtotal = subtotal * parseFloat(document.getElementById("pant").value);			
		break;
		case '/':
			subtotal = subtotal / parseFloat(document.getElementById("pant").value);
		break;
      case '=':
         subtotal = parseFloat(document.getElementById("pant").value);
      break;
		}
	
	if(premop==false)
		document.getElementById("pant").value = subtotal;
		
	premop = true;
	opAnt = op;
}
function esborra(){
   subtotal = 0;
   opAnt = '+';
   premop = true;
   document.getElementById("pant").value = subtotal;
}