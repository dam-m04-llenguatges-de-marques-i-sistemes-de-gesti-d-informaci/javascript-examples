/************************************************************************************/
// atzar.js ::: funcions per a la pàgina atzar.html
// Tria de paraules a l'atzar. Quan un valor és triat s'anota
// a la web i ja no es torna a triar
//
// Data última revisió: 07/05/2007
/************************************************************************************/
// Copyright (C) 2007  Mònica Ramírez Arceda
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
/************************************************************************************/

// Array amb els valors que es triaran a l'atzar
var items = new Array("Array","Date","String","Boolean","Math","Number","Function");
//longitud de la taula
var numitems=items.length;
//timer usat quan s'inicia l'atzar
var timer;
//números seleccionat a l'atzar
var aleat=0;

//Funció que s'executa en prémer el botó Inicia, inicia el llançament del "dau"
function inicia(){
   //Només llançarem el "dau" si aquest té valors, si l'array no està buida
   if(numitems>0){
      //Iniciem el timer, a cada milisegon es tria un ítem a l'atzar
      timer = window.setInterval(canviaitem,1);
      //Quan iniciem, no podem tornar a iniciar, només podem parar el dau
      document.getElementById("parada").disabled="";
      document.getElementById("inici").disabled="disabled";
   }
}

//Funció que s'executa en prémer el botó Para, para el llançament del "dau"
function para(){
   //Parem el timer
   window. clearInterval(timer);
   //Creem un nou element del document amb valor l'ítem que s'ha triat a l'atzar
   var noudiv = document.createElement("div");
   var noutext = document.createTextNode(items[aleat]);
   noudiv.appendChild(noutext);
   document.getElementById("fets").appendChild(noudiv);

   //Eliminem l'ítem ja escollit de la taula
   items.splice(aleat,1);
   //Regenerem la variable que conté la longitud de la taula
   numitems=items.length;
   //Quan parem, no podem tornar a parar, només podem tornar a iniciar.
   //Si no queden elements a l'array deshabilitem els dos boton.
   //No podrem ni tornar a parar ni tornar a iniciar
   if(numitems==0)
      document.getElementById("inici").disabled="disabled";
   else
      document.getElementById("inici").disabled="";
   document.getElementById("parada").disabled="disabled";
}

//Funció que tria elements de l'array aleatòriament
function canviaitem(){
   // Triem un número aleatori entre 0 i numitems
   aleat = Math.random()*(numitems-1);
   aleat = Math.round(aleat);
   // Substituïm el valor del grup segons el que hagi sortit a l'atzar
   document.getElementById("item").firstChild.replaceData(0,document.getElementById("item").firstChild.nodeValue.length,items[aleat]);
} 