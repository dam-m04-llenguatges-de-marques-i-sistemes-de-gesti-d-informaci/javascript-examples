/************************************************************************************/
// exer10.js ::: funcions per a l'exercici 10
// Crea un test de preguntes i avalua el resultat
//
// Data �ltima revisi�: 09/05/2007
/************************************************************************************/
// Copyright (C) 2007  M�nica Ram�rez Arceda
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
/************************************************************************************/

//n�mero de pregunta (n�mero de "p�gina")
var seg = -1;
//n�mero de preguntes
var numPreguntes = 5;
//n�mero de pregResp que tindr� cada pregunta
var numRespostes = 3;
//array amb les preguntes
var pregResp = new Array(numPreguntes);
//N�mero d'encerts
var encerts = 0;

//funcio que ens inicialitza la web: estableix les preguntes/pregResp i ens mostra la primera pregunta
function inicialitza(){
	iniPregResp();
	seguent();
}

function iniPregResp(){
   var i=0;
   
   //Creen array de dos dimensions.
   //Pre a cada fila: 	1a columna -> pregunta
   //					Segents columnes -> possibles respostes
   //					�ltima columna -> resposta correcta 
   for(i=0;i<numPreguntes;i++){
      pregResp[i]=new Array(numRespostes+3);
   }
   
   //Inicialitzem preguntes i respostes
   pregResp[0][0] = "Dins de quin element podem escriure codi JavaScript?";
   pregResp[0][1] = "\&lt;scripting\&gt;";
   pregResp[0][2] = "\&lt;js\&gt;";
   pregResp[0][3] = "\&lt;script\&gt;";
   pregResp[0][4] = 3;
   
   pregResp[1][0] = "Quina �s manera correcta de referir-nos a un script extern anomenat xxx.js?";
   pregResp[1][1] = "\&lt;script name='xxx.js'\&gt;"
   pregResp[1][2] = "\&lt;script href='xxx.js'\&gt;";
   pregResp[1][3] = "\&lt;script src='xxx.js'\&gt;";
   pregResp[1][4] = 3;

   pregResp[2][0] = "Com podem escriure 'Hola!' en una finestra emergent?";
   pregResp[2][1] = "alert('Hello World')";
   pregResp[2][2] = "msgBox('Hello World')";
   pregResp[2][3] = "alertBox('Hello World')";
   pregResp[2][4] = 1;
   
   pregResp[3][0] = "Com podem cridar una funci� que es diu laMevaFuncio?";
   pregResp[3][1] = "myFunction()";
   pregResp[3][2] = "call myFunction()";
   pregResp[3][3] = "call function myFunction";
   pregResp[3][4] = 1;
   
   pregResp[4][0] = "Com arrodoniries el n�mero 7.25?";
   pregResp[4][1] = "Math.rnd(7.25)";
   pregResp[4][2] = "Math.round(7.25)";
   pregResp[4][3] = "round(7.25)";
   pregResp[4][4] = 2;
	
}

//Funci� que inicialitza els controls
function iniControls(){
	var i=0;
	//Posem tots els radio button en blanc
	for (i=1;i<=numRespostes;i++){
		document.getElementById("r"+i).checked="";
		document.getElementById("r"+i).disabled="";
	}
}

function fiControls(){
	var i=0;
	//Posem tots els radio button en blanc
	for (i=1;i<=numRespostes;i++)
		document.getElementById("r"+i).disabled="disabled";
	document.getElementById("respon").disabled="disabled";
}


//Funci� que valida si hem contestat la pregunta
function validaControls(){
	var contestat = false;
	for (i=1;i<=numRespostes;i++){
		//Si s'ha contestat alguna pregunta retorna true
		if(document.getElementById("r"+i).checked)
			contestat = true;					
	}
	return contestat;
}

//Funci�que ens escriu la seg�ent pregunta amb les seves respostes
function seguent(){
   var i;
   //Passem a la seg�ent pregunta
   seg++;
   
   if (seg<numPreguntes){
   		//Escrivim la pregunta
   		document.getElementById("pregunta").innerHTML = pregResp[seg][0];
		//Escrivim les respostes
		for(i=1;i<=numRespostes;i++){
			document.getElementById("tr"+i).innerHTML = pregResp[seg][i];
		}
   		//Inicialitzem els controls
		iniControls();
	}
	else {
		//Desactivem controls del test
		fiControls();
		//Escrivim la nota treta
		document.getElementById("nota").innerHTML="Encerts: " + encerts + " Nota: " + encerts*10/numPreguntes;
		for (i=0;i<numPreguntes;i++){
			document.getElementById("solucions").innerHTML = document.getElementById("solucions").innerHTML + pregResp[i][0] + " "+ pregResp[i][pregResp[i][numRespostes+1]] + "<br />";
		}
		
		
	}
}

//Funci� que es crida quan es respon a la pregunta, emmagatzema la resposta
function respon(){
	var i = 0;
	//Nom�s emmagatzamem la resposta si s'ha contestat
	if(validaControls()==true){
		for (i=1;i<=numRespostes;i++)
			//Si la resposta �s la correcta augmentem la nota
			if(document.getElementById("r"+i).checked)
				if(pregResp[seg][numRespostes+1]==i)
					encerts++;
		//Passem a la seg�ent pregunta
		seguent();
	}
	else{
		//Si no es contesta es d�na un error i no es passa a la pregunta seg�ent 
		alert("�s obligatori contestar la pregunta!!!")
	}
}
